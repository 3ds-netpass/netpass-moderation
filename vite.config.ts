import fs from "fs";
import path from "path";
import { defineConfig } from "vite";
import svgLoader from 'vite-svg-loader'

const htmlFiles = fs.readdirSync(path.resolve(__dirname, "client")).filter(file => file.endsWith(".html"));
const entries: Record<string, string> = {};

for (const file of htmlFiles) {
    const name = file.replace(".html", "");
    entries[name] = path.resolve(__dirname, `client/${file}`);
}

export default defineConfig({
    root: "client",
    base: "/assets",
    build: {
        target: "esnext",
        outDir: "../dist/client",
        emptyOutDir: true,
        sourcemap: true,
        rollupOptions: {
            input: entries,
            output: {
                entryFileNames: "[name].js",
                chunkFileNames: "[name].js",
                assetFileNames: "[name].[ext]",
                sourcemapFileNames: "[name].js.map",
                sourcemap: true,
            },
        }
    },
    plugins: [
        svgLoader()
    ]
});