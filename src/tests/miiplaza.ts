import "dotenv/config";
import fs from 'fs';
import path from 'path';
import CecMessage from '../moderation/cec/cecMessage.js';
import CecPayloadHandler from '../moderation/cec/cecPayloadHandler.js';
import { CecMessageMiiPlaza } from '../moderation/cec/handlers/miiPlaza.js';

CecPayloadHandler.registerHandlers();

const message = new CecMessage(fs.readFileSync(path.resolve("./src/tests/payloads/miiplaza.bin")));
const payload = message.getPayload();

fs.mkdirSync(path.resolve("./src/tests/output/miiplaza"), { recursive: true });

// const mii = payload?.mii.decryptMii();
// console.log(mii?.creatorName);