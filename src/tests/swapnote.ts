import "dotenv/config";
import fs from 'fs';
import path from 'path';
import CecMessage from '../moderation/cec/cecMessage.js';
import CecPayloadHandler from '../moderation/cec/cecPayloadHandler.js';
import { CecMessageLetterbox } from "../moderation/cec/handlers/letterbox.js";

CecPayloadHandler.registerHandlers();

// const message = new CecMessage(fs.readFileSync(path.resolve("./src/tests/payloads/swapnote.bin")));
// const payload = message.getPayload<CecMessageLetterbox>();

// fs.mkdirSync(path.resolve("./src/tests/output/swapnote"), { recursive: true });

// payload?.jpegs.forEach((jpeg, i) => {
//     fs.writeFileSync(path.resolve(`./src/tests/output/swapnote/${i}.jpg`), jpeg.buffer);
// })

// console.log(`Found ${payload?.jpegs.length} images`);

// const mii = message.getMii();
// if (!mii) {
//     console.log("No Mii found");
// } else {
//     console.log(mii);
// }
