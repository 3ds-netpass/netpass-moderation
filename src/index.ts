import 'dotenv/config';
import DiscordClient from "./discord/client.js";
import Database from "./database/index.js";
import Webserver from './webserver/webserver.js';
import CecPayloadHandler from './moderation/cec/cecPayloadHandler.js';

export const Client = new DiscordClient(process.env.DISCORD_TOKEN!);
export const db = new Database();
export const qb = db.knex;
export const Server = new Webserver(parseInt(process.env.PORT!));

db.init()
CecPayloadHandler.registerHandlers()
