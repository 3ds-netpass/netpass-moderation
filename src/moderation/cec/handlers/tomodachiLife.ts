import AbstractCecHandler, { WebReportPayload, WebReportPayloadBlockField } from "./abstractCecHandler.js";
import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "../types/base.js";

export default class TomodachiLifeCecHandler implements AbstractCecHandler {
    readonly titleId: number = 0x0008C500;

    handle(payload: Buffer): WebReportPayload | undefined {
        const pl = CecMessageTomodachiLife.read(payload) as CecMessageTomodachiLife;

        return {
			titleId: this.titleId,
			blocks: [{
				type: "field",
				name: "Island name",
				value: pl.island_name,
			} as WebReportPayloadBlockField]
        };
    }
}

@CStructClass({
    classes: {
    }
})
export class CecMessageTomodachiLife extends CStructBase {
    @CStructProperty({ type: 'buf52' })
	public padding!: Buffer;
    @CStructProperty({ type: 'ws13' })
    public island_name!: string;
}
