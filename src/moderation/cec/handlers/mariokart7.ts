import AbstractCecHandler, { WebReportPayload, WebReportPayloadBlockField } from "./abstractCecHandler.js";
import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "../types/base.js";

export default class MarioKart7CecHandler implements AbstractCecHandler {
    readonly titleId: number = 0x00030600;

    handle(payload: Buffer): WebReportPayload | undefined {
        const pl = CecMessageMarioKart7.read(payload) as CecMessageMarioKart7;

        return {
			titleId: this.titleId,
            blocks: [{
				type: "field",
				name: "Greeting",
				value: pl.message,
			} as WebReportPayloadBlockField]
        };
    }
}

@CStructClass({
    classes: {
    }
})
export class CecMessageMarioKart7 extends CStructBase {
    @CStructProperty({ type: 'ws12' })
    public message!: string;
}
