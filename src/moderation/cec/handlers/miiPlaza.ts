import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import AbstractCecHandler, { WebReportPayload, WebReportPayloadBlockField, WebReportPayloadBlockList, WebReportPayloadBlockHList } from "./abstractCecHandler.js";
import ApplicationSettings from "../types/applicationSettings.js";
import CStructBase from "../types/base.js";
import ApplicationTitle from "../types/applicationTitle.js";
import MiiPayload from "../types/miiPayload.js";
import Mii from '../../../utils/mii/mii.js';

export default class MiiPlazaCecHandler implements AbstractCecHandler {
    public readonly titleId = 0x20800;

    public handle(payload: Buffer) {
        const pl = CecMessageMiiPlaza.read(payload) as CecMessageMiiPlaza;
        if (!pl) {
            return undefined;
        }
        let decryptedMii: Mii | false = false
        try {
            decryptedMii = pl.mii.decryptMii()
        } catch (e) {
            console.error("Error decrypting mii:");
            console.error(e);
        }
        const res: WebReportPayload = {
            titleId: this.titleId,
            ...(decryptedMii ? {
                mii: decryptedMii.studioUrl(),
                miiName: decryptedMii.miiName,
                miiCreator: decryptedMii.creatorName,
            } : {}),
            blocks: [
                {
                    type: "field",
                    name: "Last played game",
                    value: pl.title[1].short_description
                } as WebReportPayloadBlockField,
                {
                    type: "field",
                    name: "message",
                    value: pl.message
                } as WebReportPayloadBlockField,
                {
                    type: "list",
                    blocks: pl.reply_msg.map((msg, index) => pl.replied_msg[index] ? {
                        type: "hlist",
                        blocks: [
                            {
                                type: "field",
                                name: "message",
                                value: pl.reply_msg[index],
                            } as WebReportPayloadBlockField,
                            {
                                type: "field",
                                name: "reply to",
                                value: pl.replied_msg[index],
                            } as WebReportPayloadBlockField,
                        ]
                    } as WebReportPayloadBlockHList : { type: "none" }),
                } as WebReportPayloadBlockList
            ]
        }
        return res;
    }
}

export class CecMessageMiiPazaReplyListEntry extends CStructBase {
    @CStructProperty({ type: 'u32' })
    public mii_id!: number;
    @CStructProperty({ type: 'buf6' })
    public mac!: Buffer;
}

@CStructClass({
    classes: {
        ApplicationSettings,
        ApplicationTitle,
        MiiPayload,
        CecMessageMiiPazaReplyListEntry,
    }
})
export class CecMessageMiiPlaza extends CStructBase {
    @CStructProperty({ type: 'ApplicationSettings' })
    public app_settings!: ApplicationSettings;
    @CStructProperty({ type: 'ApplicationTitle[16]' })
    public title!: ApplicationTitle[];
    @CStructProperty({ type: 'buf' + 0x168C })
    public padding1!: Buffer;
    @CStructProperty({ type: 'MiiPayload' })
    public mii!: MiiPayload
    @CStructProperty({ type: 'buf' + 0x1C })
    public padding2!: Buffer;
    @CStructProperty({ type: 'ws' + 0x20 + '[16]' })
    public country!: string[];
    @CStructProperty({ type: 'ws' + 0x20 + '[16]' })
    public region!: string[];
    @CStructProperty({ type: 'buf16' })
    public padding3!: Buffer;
    @CStructProperty({ type: 'ws17' })
    public message!: string;
    @CStructProperty({ type: 'CecMessageMiiPazaReplyListEntry[16]' })
    public reply_list!: CecMessageMiiPazaReplyListEntry[];
    @CStructProperty({ type: 'ws' + 0x11 + '[16]' })
    public reply_msg!: string[];
    @CStructProperty({ type: 'ws' + 0x11 + '[16]' })
    public replied_msg!: string[];
}
