export default interface AbstractCecHandler {
    readonly titleId: number;
    handle(payload: Buffer): WebReportPayload | undefined;
}

export interface WebReportPayloadBlock {
    type: string;
}

export interface WebReportPayloadBlockField extends WebReportPayloadBlock {
    type: "field";
    name: string;
    value: string;
}

export interface WebReportPayloadBlockList extends WebReportPayloadBlock {
    type: "list";
    blocks: WebReportPayloadBlock[];
}

export interface WebReportPayloadBlockHList extends WebReportPayloadBlock {
    type: "hlist";
    blocks: WebReportPayloadBlock[];
}

export interface WebReportPayloadBlockImage extends WebReportPayloadBlock {
    type: "image";
    url: string;
    alt?: string;
}

export interface WebReportPayloadBlockNone extends WebReportPayloadBlock {
    type: "none";
}

export interface WebReportPayload {
    titleId: number;
    titleName?: string;
    mii?: string;
    miiName?: string;
    miiCreator?: string;
    blocks: WebReportPayloadBlock[];
}
