import AbstractCecHandler, { WebReportPayload, WebReportPayloadBlockHList, WebReportPayloadBlockImage } from "./abstractCecHandler.js";
import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "../types/base.js";
import Jpeg from "../types/jpeg.js";

export default class LetterboxCecHandler implements AbstractCecHandler {
    readonly titleId: number = 0x051600;

    handle(payload: Buffer): WebReportPayload | undefined {
        const ix = payload.indexOf(Buffer.from([0xFF, 0xD8]));
        const res = CecMessageLetterbox.read(payload, ix - 0x68 - 4) as CecMessageLetterbox;

        return {
            titleId: this.titleId,
            blocks: [{
                type: "hlist",
                blocks: res.jpegs.map(j => ({
                    type: "image",
                    url: `data:image/jpeg;base64,${j.buffer.toString('base64')}`,
                } as WebReportPayloadBlockImage))
            } as WebReportPayloadBlockHList]
        };
    }
}

@CStructClass({
    classes: {
        Jpeg,
    }
})
export class CecMessageLetterbox extends CStructBase {
    @CStructProperty({ type: 'u32' })
    public total_size!: number;
    @CStructProperty({ type: 'u32' })
    public jpeg_size!: number;
    @CStructProperty({ type: 'buf' + 0x60 })
    public padding!: Buffer;
    @CStructProperty({ type: 'Jpeg[4]' })
    public jpegs!: Jpeg[];
}
