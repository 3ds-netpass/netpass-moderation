import Mii from "../../utils/mii/mii.js";
import CecPayloadHandler from "./cecPayloadHandler.js";
import { WebReportPayload } from "./handlers/abstractCecHandler.js";
import CStructBase from "./types/base.js";
import { CecMessageExHeader } from "./types/cecMessageExHeader.js";
import { CecMessageHeader } from "./types/cecMessageHeader.js";
import MiiPayload from "./types/miiPayload.js";

export default class CecMessage {

    public header: CecMessageHeader;
    public ex_header: CecMessageExHeader[];
    public payload: Buffer;
    public hmac: Buffer;

    constructor(buf: Buffer) {
        let offset = 0;
        this.header = CecMessageHeader.read(buf, offset);
        offset += this.header._offset;
        while (offset%4) offset++;
        this.ex_header = [];
        while (offset < this.header.total_header_size - 8) {
            const ex: CecMessageExHeader = CecMessageExHeader.read(buf, offset);
            offset = ex._offset;
            this.ex_header.push(ex);
            while (offset%4) offset++;
        }
        this.payload = buf.slice(offset, this.header.message_size - 0x20);
        this.hmac = buf.slice(this.header.message_size - 0x20, this.header.message_size);

    }


    public getMii(): Mii | undefined {
        const payloadIndex = this.payload.indexOf('CFPB');
        if (payloadIndex == -1) {
            return undefined;
        }
        try {
            const payload: MiiPayload = MiiPayload.read(this.payload, payloadIndex);
            return payload.decryptMii();
        } catch {
            return undefined;
        }
    }

    public getPayload(): WebReportPayload | undefined {
        try {
            const handler = CecPayloadHandler.getHandler(this.header.title_id);
            if (handler) {
                return handler.handle(this.payload)
            }
        } catch (e) {
            console.error(e);
            return undefined;
        }

        console.log(`No handler for title_id: ${this.header.title_id.toString(16)}`);

        return undefined
    }
}
