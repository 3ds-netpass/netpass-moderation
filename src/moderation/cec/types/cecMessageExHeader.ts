import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";

export class CecMessageExHeader extends CStructBase {
    @CStructProperty({ type: 'u32' })
    public type!: number;
    @CStructProperty({ type: 'buf[u32]' }) // reads 4 bytes as size and then the buffer
    public data!: Buffer;
}
