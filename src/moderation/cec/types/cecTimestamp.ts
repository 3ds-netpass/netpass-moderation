import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";

export default class CecTimestamp extends CStructBase {
    @CStructProperty({ type: 'u32' })
    public year!: number;
    @CStructProperty({ type: 'u8' })
    public month!: number;
    @CStructProperty({ type: 'u8' })
    public day!: number;
    @CStructProperty({ type: 'u8' })
    public weekday!: number;
    @CStructProperty({ type: 'u8' })
    public hour!: number;
    @CStructProperty({ type: 'u8' })
    public minute!: number;
    @CStructProperty({ type: 'u8' })
    public second!: number;
    @CStructProperty({ type: 'u16' })
    public millisecond!: number;
}
