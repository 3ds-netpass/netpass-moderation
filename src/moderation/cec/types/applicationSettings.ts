import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";
import RegionalGameAgeRating from "./regionalGameAgeRating.js";


@CStructClass({
    classes: {
        RegionalGameAgeRating,
    }
})
export default class ApplicationSettings extends CStructBase {
    @CStructProperty({ type: 'RegionalGameAgeRating' })
    public age_rating!: RegionalGameAgeRating;
    @CStructProperty({ type: 'u32' })
    public region_lockout!: number;
    @CStructProperty({ type: 'u32' })
    public match_maker_id!: number;
    @CStructProperty({ type: 'u64' })
    public match_maker_bit_id!: bigint;
    @CStructProperty({ type: 'u32' })
    public flags!: number;
    @CStructProperty({ type: 'u16' })
    public eula_version!: number;
    @CStructProperty({ type: 'u16' })
    public padding!: number;
    @CStructProperty({ type: 'f' })
    public optimal_anim_frame!: number;
    @CStructProperty({ type: 'u32' })
    public cec_title_id!: number;
}
