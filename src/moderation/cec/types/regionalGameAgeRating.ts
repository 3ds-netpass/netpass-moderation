import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";

export default class RegionalGameAgeRating extends CStructBase {
    @CStructProperty({ type: 'u8' })
    public japan!: number;
    @CStructProperty({ type: 'u8' })
    public usa!: number;
    @CStructProperty({ type: 'u8' })
    public padding1!: number;
    @CStructProperty({ type: 'u8' })
    public germany!: number;
    @CStructProperty({ type: 'u8' })
    public europe!: number;
    @CStructProperty({ type: 'u8' })
    public padding2!: number;
    @CStructProperty({ type: 'u8' })
    public portugal!: number;
    @CStructProperty({ type: 'u8' })
    public england!: number;
    @CStructProperty({ type: 'u8' })
    public australia!: number;
    @CStructProperty({ type: 'u8' })
    public south_korea!: number;
    @CStructProperty({ type: 'u8' })
    public taiwan!: number;
    @CStructProperty({ type: 'buf5' })
    public padding3!: Buffer;
}
