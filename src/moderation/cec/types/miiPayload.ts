import crypto from 'crypto';
import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";
import Mii from '../../../utils/mii/mii.js';

export default class MiiPayload extends CStructBase {
    @CStructProperty({ type: 'u32' })
    public magic!: number;
    @CStructProperty({ type: 'buf20' })
    public unknown!: Buffer;
    @CStructProperty({ type: 'buf8' })
    public nonce!: Buffer;
    @CStructProperty({ type: 'buf88' })
    public ciphertext!: Buffer;
    @CStructProperty({ type: 'buf16' })
    public mac!: Buffer;

    public decryptMii(): Mii {
        if (this.magic != 0x42504643) { // "CFPB"
            throw new Error("CFPB magic is invalid " + this.magic);
        }
        const key = Buffer.from(process.env.MII_DECRYPT_KEY ?? '', 'hex');
        const cipher = crypto.createCipheriv('aes-128-ccm', key, Buffer.concat([this.nonce, Buffer.from([0, 0, 0, 0])]), {
            authTagLength: 16,
        });

        const chunks = cipher.update(this.ciphertext);
        cipher.final()
        const plaintext = Buffer.concat([chunks.slice(0, 12), this.nonce, chunks.slice(12, 88)]);
        return new Mii(plaintext);
    }
}