import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";

export default class Jpeg extends CStructBase {
    @CStructProperty({ type: 'buf[u32%4]' })
    public buffer!: Buffer;
}

