import { CStructLE } from '@mrhiden/cstruct';

export default class CStructBase {
	public _size!: number;
	public _offset!: number;
	public static read<T extends CStructBase>(buf: Buffer, offset?: number): T {
		const obj = CStructLE.read(this, buf, offset);
		const ret = obj.struct as T;
		ret._size = obj.size;
		ret._offset = obj.offset;
		return ret;

	}
}
