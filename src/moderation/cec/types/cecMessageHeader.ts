import { CStructClass, CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";
import CecTimestamp from "./cecTimestamp.js";

@CStructClass({
    classes: {
        CecTimestamp,
    }
})
export class CecMessageHeader extends CStructBase {
    @CStructProperty({ type: 'u16' })
    public magic!: number;
    @CStructProperty({ type: 'u16' })
    public padding!: number;
    @CStructProperty({ type: 'u32' })
    public message_size!: number;
    @CStructProperty({ type: 'u32' })
    public total_header_size!: number;
    @CStructProperty({ type: 'u32' })
    public body_size!: number;
    @CStructProperty({ type: 'u32' })
    public title_id!: number;
    @CStructProperty({ type: 'u32' })
    public title_id2!: number;
    @CStructProperty({ type: 'u32' })
    public batch_id!: number;
    @CStructProperty({ type: 'u32' })
    public unknown_1!: number;
    @CStructProperty({ type: 'u64' })
    public message_id!: bigint;
    @CStructProperty({ type: 'u32' })
    public message_version!: number;
    @CStructProperty({ type: 'u64' })
    public message_id2!: bigint;
    @CStructProperty({ type: 'u8' })
    public flags!: number;
    @CStructProperty({ type: 'u8' })
    public send_method!: number;
    @CStructProperty({ type: 'bool' })
    public unopened!: boolean;
    @CStructProperty({ type: 'bool' })
    public new_flag!: boolean;
    @CStructProperty({ type: 'u64' })
    public sender_id!: bigint;
    @CStructProperty({ type: 'u64' })
    public sender_id2!: bigint;
    @CStructProperty({ type: 'CecTimestamp' })
    public sent!: CecTimestamp;
    @CStructProperty({ type: 'CecTimestamp' })
    public received!: CecTimestamp;
    @CStructProperty({ type: 'CecTimestamp' })
    public created!: CecTimestamp;
    @CStructProperty({ type: 'u8' })
    public send_cofunctionunt!: number;
    @CStructProperty({ type: 'u8' })
    public forward_count!: number;
    @CStructProperty({ type: 'u16' })
    public user_data!: number;
}
