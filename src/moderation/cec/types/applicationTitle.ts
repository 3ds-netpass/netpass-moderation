import { CStructProperty } from "@mrhiden/cstruct";
import CStructBase from "./base.js";

export default class ApplicationTitle extends CStructBase {
    @CStructProperty({ type: 'ws' + 0x40 })
    public short_description!: string;
    @CStructProperty({ type: 'ws' + 0x80 })
    public long_description!: Buffer;
    @CStructProperty({ type: 'ws' + 0x40 })
    public publisher!: Buffer;
}
