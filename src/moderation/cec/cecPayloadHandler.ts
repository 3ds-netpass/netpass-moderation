import AbstractCecHandler from "./handlers/abstractCecHandler.js";
import LetterboxCecHandler from "./handlers/letterbox.js";
import MiiPlazaCecHandler from "./handlers/miiPlaza.js";
import MarioKart7CecHandler from "./handlers/mariokart7.js";
import TomodachiLifeCecHandler from "./handlers/tomodachiLife.js";

export default class CecPayloadHandler {

    public static handlers: Record<number, AbstractCecHandler> = {}

    public static registerHandlers() {
        this.registerHandler(new MiiPlazaCecHandler())
        this.registerHandler(new LetterboxCecHandler())
        this.registerHandler(new MarioKart7CecHandler())
        this.registerHandler(new TomodachiLifeCecHandler())
    }

    public static registerHandler(handler: AbstractCecHandler) {
        CecPayloadHandler.handlers[handler.titleId] = handler
    }

    public static getHandler(titleId: number) {
        return CecPayloadHandler.handlers[titleId]
    }

}