import { Inbox, Report, Title, Ban } from "../database/schema.js";
import { db, qb } from "../index.js";
import CecMessage from "./cec/cecMessage.js";
import fs from 'fs';
import path from 'path';
import { WebReportPayload } from "./cec/handlers/abstractCecHandler.js";
import { raw } from "express";

export default class Moderation {

    public static async getReportCount() {
        return parseInt((await qb<Report>("reports").where('closed', false).count())[0].count as string)
    }

    public static async getInboxCount() {
        return parseInt((await qb<Inbox>("inbox").count())[0].count as string)
    }

    public static async getReports(limit: number = 25, page: number = 1, includeClosed: boolean = false) {
        const query = qb<Report>("reports")

        if (!includeClosed) {
            query.where("closed", false)
        }

        return query.limit(limit).offset((page - 1) * limit)
    }

    public static async getRawReport(id: bigint) {
        const report = await qb<Report>("reports").where("id", id).first()
        return report
    }

    public static async getReport(id: bigint) {
        const report = await qb<Report>("reports").where("id", id).first()

        if (!report) {
            return null
        }

        // for testing
        // report.messages = [fs.readFileSync(path.resolve(`./src/tests/payloads/miiplaza.bin`))]

        const messages = await Promise.all(report.messages.map(async (m) => {
            try {
                const message = new CecMessage(m);
                const pl = message.getPayload();
                const mii = message.getMii();
                return {
                    ...(pl ? pl : {}),
                    ...(mii ? {
                        mii: mii.studioUrl(),
                        miiCreator: mii.creatorName,
                        miiName: mii.miiName,
                    } : {}),
                    titleId: message.header.title_id,
                    titleName: await Moderation.getTitleFromId(message.header.title_id)
                }
            } catch (e) {
                return null
            }
        }))

        return {
            ...report,
            messages
        }

    }

    public static async ban(mac: bigint, reason: string, interval?: string, notes?: string) {
        await qb<Ban>("bans").insert({mac, reason, notes, time_end: interval ? qb.raw(`now() + (?::INTERVAL)`, interval) : null})
    }

    public static async closeReportsByMac(mac: bigint) {
        await qb<Report>("reports").update('closed', true).where('reported_mac', mac).orWhere('from_mac', mac)
    }

    public static async closeReport(id: bigint) {
        await qb<Report>("reports").update('closed', true).where('id', id)
    }

    public static async getTitleFromId(id: number) {
        return (await qb<Title>("title").where("title_id", id).orderBy('count', 'desc').first())?.title_name
    }
}
