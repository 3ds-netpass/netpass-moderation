import { NextFunction, Request, Response } from "express";
import MemberLookup from "../../discord/lookup.js";
import path from "path";

export default async function (req: Request, res: Response, next: NextFunction) {
    const user = req.body.session?.user
    if (!user) {
        res.status(403).sendFile(path.resolve("./dist/client/not_authorized.html"))
        return
    }

    const lookupInfo = await MemberLookup.lookupMember(user.id)

    if (!lookupInfo) {
        res.sendStatus(500) // User not found
        return
    }

    if (!lookupInfo.isMod) {
        res.status(403).sendFile(path.resolve("./dist/client/not_authorized.html"))
        return
    }

    next();
}
