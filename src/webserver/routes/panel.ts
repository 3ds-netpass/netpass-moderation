import { Router } from 'express';
import path from 'path';
import authMiddleware from '../middleware/auth.middleware.js';
import modCheckMiddleware from '../middleware/modCheck.middleware.js';

const PanelRouter = Router();

PanelRouter.use(authMiddleware)
PanelRouter.use(modCheckMiddleware)

PanelRouter.get("/", (req, res) => {
    res.sendFile(path.resolve("./dist/client/panel.html"))
})

PanelRouter.get("/report", (req, res) => {
    res.sendFile(path.resolve("./dist/client/report.html"))
})

export default PanelRouter;
