import { Router } from 'express';
import { User } from '../../database/schema.js';
const MeRouter = Router();

MeRouter.get("/", (req, res) => {
    const user = req.body.session?.user as User

    if (!user) {
        res.redirect("/")
    }

    res.json({
        user: {
            id: user.id,
            username: user.username,
            avatar: user.avatar
        }
    })
})

MeRouter.get("/avatar", (req, res) => {
    const user = req.body.session?.user as User

    if (!user) {
        res.sendStatus(403)
        return
    }

    res.redirect(`https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png`)
})

export default MeRouter;
