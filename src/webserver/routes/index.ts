import { Router } from 'express';
import AuthMiddleware from '../middleware/auth.middleware.js';
import MemberLookup from '../../discord/lookup.js';
import path from 'path';

import AuthRouter from '../auth/authRouter.js';
import MeRouter from './me.js';
import PanelRouter from './panel.js';
import ApiRouter from './api.js';
import { User } from '../../database/schema.js';

const IndexRouter = Router();

IndexRouter.use(AuthMiddleware)
IndexRouter.use('/auth', AuthRouter);
IndexRouter.use('/me', MeRouter);
IndexRouter.use('/panel', PanelRouter);
IndexRouter.use('/api', ApiRouter);

IndexRouter.get("/", async (req, res) => {
    const user = req.body.session?.user as User

    if (!user) {
        res.sendFile(path.resolve("./dist/client/index.html"))
        return
    }

    const lookupInfo = await MemberLookup.lookupMember(user.id)


    if (!lookupInfo) {
        res.sendStatus(500) // User not found
        return
    }

    if (!lookupInfo.isMod) {
        res.status(403).sendFile(path.resolve("./dist/client/not_authorized.html"))
        return
    }

    res.redirect("/panel")
})

export default IndexRouter;
