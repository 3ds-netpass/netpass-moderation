import { Router } from 'express';
import authMiddleware from '../middleware/auth.middleware.js';
import modCheckMiddleware from '../middleware/modCheck.middleware.js';
import Moderation from '../../moderation/index.js';
import { Report } from '../../database/schema.js';
import Utils from '../../utils/Utils.js';

const ApiRouter = Router();

ApiRouter.use(authMiddleware)
ApiRouter.use(modCheckMiddleware)

ApiRouter.get("/summary", async (req, res) => {

    const [passes, reports] = await Promise.all([
        Moderation.getInboxCount(),
        Moderation.getReportCount()
    ])

    res.json({
        passes,
        reports
    })
})

ApiRouter.get("/reports", async (req, res) => {
    const q = req.query

    const limit = q.limit ? parseInt(q.limit as string) : 10
    const page = q.page ? parseInt(q.page as string) : 1

    const reports = await Moderation.getReports(limit, page)

    res.json({
        reports: reports.map((r: Report) => {
            const res = {
                ...r as any,
                messages: r.messages.length,
                from_mac: Utils.macToString(r.from_mac),
                from_mac_int: r.from_mac,
                reported_mac: Utils.macToString(r.reported_mac),
                reported_mac_int: r.reported_mac
            }
            return res
        })
    })
})

ApiRouter.get("/report/:id", async (req, res) => {
    const id = BigInt(req.params.id)

    const report = await Moderation.getReport(id)

    if (!report) {
        return res.status(404).json({
            error: "Report not found"
        })
    }

    res.json({
        ...report,
        // messages: report.messages.length,
        from_mac: Utils.macToString(report.from_mac),
        from_mac_int: report.from_mac,
        reported_mac: Utils.macToString(report.reported_mac),
        reported_mac_int: report.reported_mac
    })
})

ApiRouter.get("/report/:id/close", async (req, res) => {
    const id = BigInt(req.params.id)
    await Moderation.closeReport(id)
    res.status(200).json({
        success: true
    })
})

ApiRouter.post("/ban/:mac", async (req, res) => {
    const mac = BigInt(req.params.mac)
    try {
        await Moderation.ban(mac, req.body.ban_reason, req.body.ban_forever ? null : req.body.ban_length, req.body.ban_notes);
        await Moderation.closeReportsByMac(mac);
        return res.status(200).json({
            success: true
        })
    } catch(e) {
        return res.status(500).json({
            error: "internal server error"
        })
    }
})

ApiRouter.get("/report/:id/message/:msgId", async (req, res) => {
    const reportId = BigInt(req.params.id)
    const messageId = parseInt(req.params.msgId)

    const report = await Moderation.getRawReport(reportId)

    if (!report) {
        return res.status(404).json({
            error: "Report not found"
        })
    }

    const message = report.messages[messageId]

    if (!message) {
        return res.status(404).json({
            error: "Message not found"
        })
    }

    res
        .setHeader('Content-Type', 'octet/stream')
        .setHeader('Content-Disposition', `attachment; filename="netpass_report_${reportId}_${messageId}.bin"`)
        .send(message)
})

export default ApiRouter;
