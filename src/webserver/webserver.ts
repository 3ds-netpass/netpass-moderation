import http from 'http';
import express from 'express';
import path from 'path';
import IndexRouter from './routes/index.js';
import { Logger } from '../utils/logger.js';
import cookieParser from 'cookie-parser';

export default class Webserver {

    private app = express();
    private server = http.createServer(this.app);
    public logger = new Logger("Webserver")

    constructor(public readonly port: number) {

        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(cookieParser());
        this.app.disable('x-powered-by');

        this.app.use("/assets", express.static(path.resolve("dist/client")));
        this.app.use(IndexRouter)

        this.server.listen({
            host: process.env.HOST || '0.0.0.0',
            port: port
        }, () => {
            this.logger.log(`Server listening on port ${port}`)
        })

    }

    public close(): void {
        this.server.close()
    }

    public get expressApp(): express.Application {
        return this.app
    }

    public get httpServer(): http.Server {
        return this.server
    }

}
