import { Router } from 'express';
import DiscordOauthProvider from './provider/discordOauthProvider.js';

const AuthRouter = Router();

const discordOauth = new DiscordOauthProvider()

AuthRouter.get("/", (req, res) => {
    res.redirect(discordOauth.generateOauthUrl())
})

AuthRouter.get("/callback", (req, res) => {
    discordOauth.handleCallback(req, res)
})

AuthRouter.get("/logout", (req, res) => {
    res.clearCookie("session")
    res.redirect("/")
})

export default AuthRouter;
