import { Request, Response } from "express"
import oAuthProvider from "./oauthProvider.js"
import { db, qb } from "../../../index.js"
import SessionManager from "../manager/sessionManager.js"
import { User } from "../../../database/schema.js"

export default class DiscordOauthProvider extends oAuthProvider {

    constructor() {
        super()
    }

    public generateOauthUrl() {
        const url = new URL("https://discord.com/api/oauth2/authorize")
        url.searchParams.append("client_id", process.env.DISCORD_CLIENT_ID!)
        url.searchParams.append("redirect_uri", process.env.DISCORD_REDIRECT_URI!)
        url.searchParams.append("response_type", "code")
        url.searchParams.append("state", this.generateState())
        url.searchParams.append("scope", "identify")
        return url.toString()
    }

    public async handleCallback(req: Request, res: Response) {
        const code = req.query.code as string
        const state = req.query.state as string

        if (!code || !state) {
            res.status(400).send("Missing code or state")
            return
        }

        if (!this.isValidState(state)) {
            res.status(400).send("Invalid state")
            return
        }

        const token = await this.exchangeCode(code)


        if (!token.access_token) {
            res.status(400).send("Invalid code")
            return
        }

        const user = await this.getUser(token.access_token)

        if (!user.id) {
            res.status(400).send("Invalid token")
            return
        }

        let userEntity = await qb<User>("users").where("id", user.id).first()

        if (!userEntity) {
            userEntity = (await qb<User>("users").insert({
                id: user.id,
                username: user.username,
                avatar: user.avatar,
                token: token.access_token,
                refresh_token: token.refresh_token,
                expires_at: new Date(Date.now() + token.expires_in * 1000),
                scopes: ["identify"]
            }).returning("*"))[0]
        } else {
            userEntity = (await qb<User>("users").where("id", user.id).update({
                token: token.access_token,
                refresh_token: token.refresh_token,
                expires_at: new Date(Date.now() + token.expires_in * 1000),
            }).returning("*"))[0]
        }

        const session = await this.genSession(userEntity)
        console.log(session)

        res.cookie("session", session.id, { httpOnly: true, maxAge: 604800000 })
        res.redirect("/")
    }

    public async exchangeCode(code: string) {
        const url = new URL("https://discord.com/api/oauth2/token")
        const response = await fetch(url.toString(), {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: new URLSearchParams({
                client_id: process.env.DISCORD_CLIENT_ID!,
                client_secret: process.env.DISCORD_CLIENT_SECRET!,
                grant_type: "authorization_code",
                code,
                redirect_uri: process.env.DISCORD_REDIRECT_URI!,
            })
        })
        const json = await response.json()
        return json as {
            access_token: string
            expires_in: number
            refresh_token: string
            scope: string
            token_type: string
        }
    }

    public async getUser(token: string) {
        const url = new URL("https://discord.com/api/users/@me")
        const response = await fetch(url.toString(), {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })

        const json = await response.json()
        return json as {
            id: string
            username: string
            discriminator: string
            avatar: string
        }
    }

    public async genSession(user: User) {
        const session = await SessionManager.genSession(user)
        return session
    }

}