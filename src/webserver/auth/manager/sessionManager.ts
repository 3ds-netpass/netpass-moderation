import { Session, User } from "../../../database/schema.js";
import { db, qb } from "../../../index.js";
import { id } from "../../../utils/id.js";

const sessionExpire = 604800000;

export interface PopulatedSession {
  id: string;
  user_id: string;
  last_used: Date;
  user: User;
}

export default class SessionManager {
  public static async genSession(user: User): Promise<Session> {
    const session: Session = {
      id: id(),
      user_id: user.id,
      last_used: new Date(),
    }

    await qb<Session>("sessions").insert(session);

    return session;
  }

  public static async checkSession(session: string): Promise<PopulatedSession | undefined> {
    this.checkSessionExpire();

    // check if session exists
    const validSession = await qb<Session>("sessions").where("id", session).first()

    if (validSession) {
      // Update the session
      validSession.last_used = new Date();
      await qb<Session>("sessions").where("id", session).update(validSession);
      const user = await qb<User>("users").where("id", validSession.user_id).first();

      if (user) {
        return {
          id: validSession.id,
          user_id: validSession.user_id,
          last_used: validSession.last_used,
          user,
        }
      }
    }

    return undefined;
  }

  public static async deleteSession(session: string): Promise<void> {
    await qb<Session>("sessions").delete().where("id", session);
  }

  public static async touchSession(session: Session): Promise<void> {
    qb<Session>("sessions").where("id", session.id).update({
      last_used: new Date(),
    });
  }

  public static async checkSessionExpire(): Promise<void> {
    await qb<Session>("sessions").delete().where("last_used", "<", new Date(Date.now() - sessionExpire));
  }
}
