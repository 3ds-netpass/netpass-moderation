import { Client, IntentsBitField } from "discord.js"
import { Logger } from "../utils/logger.js"

export default class DiscordClient {
    private _client: Client
    private logger = new Logger("DiscordClient")

    constructor(private readonly token: string) {
        this._client = new Client({
            intents: [
                IntentsBitField.Flags.Guilds,
                IntentsBitField.Flags.GuildMembers,
                IntentsBitField.Flags.GuildMessages,
            ]
        })

        this._client.on("ready", () => {
            this.logger.info(`Logged in as ${this._client.user?.username}`)
        })

        this._client.login(this.token).catch((err) => {
            this.logger.error("Failed to login to Discord")
            this.logger.error(err)
            process.exit(1)
        })
    }

    public get client(): Client {
        return this._client
    }

    public async close(): Promise<void> {
        await this._client.destroy()
    }
}
