import { Client as Bot } from "../index.js";
import { Logger } from "../utils/logger.js";

export default class MemberLookup {

    private static _lookupCache: Record<string, { displayeName: string, avatar: string, isMod: boolean, cachedAt: number }> = {}
    public static logger = new Logger("MemberLookup")

    static async lookupMember(id: string) {
        if (this._lookupCache[id] && this._lookupCache[id].cachedAt > Date.now() - 60000) {
            return this._lookupCache[id];
        }

        // Lookup the member
        const member = await this.lookupMemberFromDiscord(id);
        if (!member) return this.logger.error("Failed to lookup member");

        // Cache the member
        this._lookupCache[id] = member;

        return member;
    }

    private static async lookupMemberFromDiscord(id: string) {
        const guild = Bot.client.guilds.resolve(process.env.DISCORD_GUILD_ID!)
        if (!guild) return this.logger.error("Failed to resolve guild");

        const member = guild.members?.cache.get(id) || await guild.members?.fetch(id);
        if (!member) return this.logger.error("Failed to resolve member");
        const allowedRoleIds = process.env.DISCORD_ALLOWED_ROLES!.split(",")
        const isMod = member.roles.cache.some(role => allowedRoleIds.includes(role.id))

        return {
            displayeName: member.displayName,
            avatar: member.user.displayAvatarURL({ forceStatic: true, extension: "webp" }),
            isMod,
            cachedAt: Date.now()
        }
    }

}