import cuid2 from "@paralleldrive/cuid2";

export const id = cuid2.init({
    fingerprint: "netpass!",
    length: 10,
})
