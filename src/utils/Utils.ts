export default class Utils {

    // .toString(16) only works up to 2^53
    // https://stackoverflow.com/questions/18626844/convert-a-large-integer-to-a-hex-string-in-javascript
    public static bigIntToHexString(bigInt: bigint): string {
        let dec = bigInt.toString().split('')
        let sum = [] as number[]
        let hex = [] as string[]
        let s = 0
        while (dec.length) {
            s = parseInt(dec.shift() as string)
            for (let i = 0; s || i < sum.length; i++) {
                s += (sum[i] || 0) * 10
                sum[i] = s % 16
                s = (s - sum[i]) / 16
            }
        }
        while (sum.length) {
            hex.push(sum.pop()!.toString(16))
        }
        return hex.join('')
    }

    public static macToString(mac: bigint): string {
        const hex = Utils.bigIntToHexString(mac)

        const parts = []
        for (let i = 0; i < hex.length; i += 2) {
            parts.push(hex.slice(i, i + 2))
        }

        return parts.join(":")
    }

}
