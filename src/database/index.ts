import knex from "knex";
import knexConfig from "./config.js";
import { Logger } from "../utils/logger.js";

export default class Database {

    private logger = new Logger("Database")
    public knex = knex(knexConfig)

    public async init(): Promise<void> {

        if (!await this.knex.schema.hasTable("users")) {
            await this.knex.schema
                .createTable("users", (table) => {
                    table.string("id").primary()
                    table.string("username")
                    table.string("avatar")
                    table.string("token")
                    table.string("refresh_token")
                    table.timestamp("created_at").defaultTo(this.knex.fn.now())
                    table.timestamp("expires_at")
                    table.string("scopes")
                })
        }

        if (!await this.knex.schema.hasTable("sessions")) {
            await this.knex.schema
                .createTable("sessions", (table) => {
                    table.string("id").primary()
                    table.string("user_id").references("users.id")
                    table.timestamp("last_used")
                })
        }



        this.logger.info("Database initialized")
    }

}