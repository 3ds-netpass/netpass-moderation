// this file contains the schema for the database
// not used for creating tables, but for defining the types of the data
// see /src/database/index.ts for table creation

export interface Ban {
    mac: bigint;
    time_end: Date | null;
    reason: string;
    notes: string;
}

export interface Report {
    id: bigint;
    from_mac: bigint;
    reported_mac: bigint;
    report_message: string;
    messages: Buffer[];
    closed: boolean
    time: Date;
}

export interface Session {
    id: string;
    user_id: string;
    last_used: Date;
}

export interface User {
    id: string;
    username: string;
    avatar: string;
    token: string;
    refresh_token?: string;
    created_at: Date;
    expires_at?: Date;
    scopes: string[];
}

export interface Title {
    title_id: number;
    title_name: string;
    count: number;
}

// don't need any of the types, just the interface for counts
export interface Inbox { } 
