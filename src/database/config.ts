import { Knex } from "knex";

export default {
    client: 'pg',
    connection: process.env.DATABASE_URL,
    migrations: {
        tableName: 'knex_migrations',
        directory: './migrations'
    },
} as Knex.Config