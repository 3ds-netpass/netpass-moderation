#! /bin/bash

# silly script to load non-packaged dependencies
rm -rf lib
mkdir -p lib
cd ./lib

# cstruct

git clone https://github.com/Sorunome/cstruct.git
cd cstruct
git switch soru/preserve-class
npm install
npm run build
cd ..
npm install ./cstruct

# end cstruct
