FROM node:21

WORKDIR /app

COPY . /app
RUN npm run install
RUN npm run docker:install

ENV NODE_ENV=production
ENV DOCKER_ENV=true

CMD ["npm", "run", "docker:start"]
