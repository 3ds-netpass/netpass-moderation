import "./modules/reportLoader"
import "./modules/sections"
import Util from "./modules/util"

fetch("/api/summary").then(res => res.json()).then(data => {
    const passCount = document.getElementById("pass-count") as HTMLParagraphElement
    const reportCount = document.getElementById('report-count') as HTMLParagraphElement;

    passCount.innerText = Util.kFormat(data.passes)
    reportCount.innerText = Util.kFormat(data.reports)
})
