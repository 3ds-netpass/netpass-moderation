import Util from "./modules/util";
import { ClientReport, WebReportPayload } from "./types/report";

function parseBlocks(blocks) {
    let ret = "";
    for (const block of blocks) {
        if (block) {
            ret += parseBlock(block);
        }
    }
    return ret;
}

function parseBlock(block) {
    if (!block || !block.type) {
        return "";
    }
    switch (block.type) {
        case "field":
            return `<p><strong>${Util.escapeHtml(block.name)}</strong> ${Util.escapeHtml(block.value)}</p>`;
        case "image":
            return `<p><img src="${Util.escapeHtml(block.url)}" alt="${block.alt ? Util.escapeHtml(alt) : "Image"}" /></p>`;
        case "list":
            return `<p style="display: flex;flex-wrap: nowrap;flex-direction: column">${parseBlocks(block.blocks)}</p>`;
        case "hlist":
            return `<p style="display: flex;flex-wrap: nowrap;flex-direction: row">${parseBlocks(block.blocks)}</p>`;
        case "none":
        default:
            return "";
    }
}

window.addEventListener("DOMContentLoaded", async () => {

    const hash = window.location.hash;
    const reportID = parseInt(hash.substring(1));

    if (hash && !isNaN(reportID)) {
        const res = await fetch(`/api/report/${reportID}`);
        const data = await res.json() as ClientReport;

        const reportDiv = document.getElementById('report') as HTMLDivElement;

        reportDiv.innerHTML = `
            <h1>Report on ${Util.escapeHtml(data.reported_mac)}</h1>
            <p>Reported by ${Util.escapeHtml(data.from_mac)} at ${Util.escapeHtml(Util.getFormattedDate(new Date(data.time)))}</p>
            <p>${Util.escapeHtml(data.report_message)}</p>
            ${data.messages.map((msg, i) => `
            <br>
                <h2>Message ${i + 1}: </h2>
                ${msg ? `<p>From: ${Util.escapeHtml(msg.titleName)} (${Util.escapeHtml(msg.titleId)})</p>
                ${msg.miiName ? `<p>Mii Name: ${Util.escapeHtml(msg.miiName)}</p>` : ''}
                ${msg.creatorName ? `<p>Creator: ${Util.escapeHtml(msg.creatorName)}</p>` : ''}
                ${msg.mii ? `<img src="${Util.escapeHtml(msg.mii)}" alt="Mii">` : ''}
                <br><br>
                ${msg.blocks ? parseBlocks(msg.blocks) : ''}
                <br>` : 'Error: Could not parse message<br>'}
                <button onclick="window.location.href = '/api/report/${reportID}/message/${i}'">Download</button>
            `).join('<br>')}

        <form action="/api/report/${reportID}/close" method="get">
            <input type="submit" value="close report" />
        </form>

        <form action="/api/ban/${Util.escapeHtml(data.reported_mac_int)}" method="post">
            <h2>Ban reported user ${Util.escapeHtml(data.reported_mac)}</h2>
            <p><label for="ban_length">Ban length (e.g. '1 month', '2 weeks'):</label> <input type="string" name="ban_length" /></p>
            <p><label for="ban_forever">Ban forever:</label> <input type="checkbox" name="ban_forever" /></p>
            <p><label for="ban_reason">Ban reason:</label> <input type02string" name="ban_reason" /></p>
            <p><label for="ban_notes">Notes (internally only):</label><br><textarea name="ban_notes"></textarea></p>
            <p><input type="submit" value="ban" /></p>
        </form>

        <details>
        <summary>Ban reporter</summary>
        <form action="/api/ban/${Util.escapeHtml(data.from_mac_int)}" method="post">
            <h2>Ban reporting user ${Util.escapeHtml(data.from_mac)}</h2>
            <p><label for="ban_length">Ban length (e.g. '1 month', '2 weeks'):</label> <input type="string" name="ban_length" /></p>
            <p><label for="ban_forever">Ban forever:</label> <input type="checkbox" name="ban_forever" /></p>
            <p><label for="ban_reason">Ban reason:</label> <input type02string" name="ban_reason" /></p>
            <p><label for="ban_notes">Notes (internally only):</label><br><textarea name="ban_notes"></textarea></p>
            <p><input type="submit" value="ban" /></p>
        </form>
        </details>
        `;

        const messages = document.createElement('div');


        reportDiv.appendChild(messages);

    }

})