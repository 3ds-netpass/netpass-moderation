import { ClientReport } from "../types/report";
import Util from "./util";

const reportDiv = document.getElementById('report-list') as HTMLDivElement;

function createReport(id: number, mac: string, date: Date, text: string) {
    const report = document.createElement('div');
    report.className = 'report';
    report.innerHTML = `
        <p>${mac}</p>
        <p>${Util.getFormattedDate(date)}</p>
        <p>${text}</p>
    `;

    report.onclick = (ev) => {
        // This is a dummy function, replace with actual report opening
        window.location.href = `/panel/report#${id}`;
    }
    return report;
}

async function loadReports() {
    const res = await fetch('/api/reports?limit=1000');
    const data = await res.json() as {
        reports: ClientReport[]
    }
    for (const report of data.reports) {
        reportDiv.appendChild(createReport(Number(report.id), report.reported_mac, new Date(report.time), report.report_message));
    }
}

loadReports();
