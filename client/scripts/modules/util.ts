export default class Util {
    // 2024-05-11 | 5:30 AM
    public static getFormattedDate(date: Date): string {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} | ${date.getHours()}:${date.getMinutes().toString().padStart(2, "0")} ${date.getHours() >= 12 ? 'PM' : 'AM'}`;
    }

    public static kFormat(num: number) {
        const letters = ['k', 'M', 'B', 'T'];
        for (let i = letters.length - 1; i >= 0; i--) {
            const size = Math.pow(10, (i + 1) * 3);
            if (size <= num) {
                return (num / size).toFixed(2) + letters[i];
            }
        }

        return num.toString();
    }

    public static escapeHtml(unsafe): string {
        return String(unsafe)
             .replace(/&/g, "&amp;")
             .replace(/</g, "&lt;")
             .replace(/>/g, "&gt;")
             .replace(/"/g, "&quot;")
             .replace(/'/g, "&#039;");
     }
}
