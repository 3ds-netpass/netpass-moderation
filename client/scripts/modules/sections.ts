import NetPassLogo from "../../img/netpass-logo.svg?url";
import NetPassLogoNoText from "../../img/netpass-logo-notext.svg?url";

var username = "@discordusername";
let sections = ["overview", "netpass-users", "reports", "activity-log"];

const showSectionButtons: Record<number, HTMLButtonElement> = {
    1: document.getElementById("overview-button") as HTMLButtonElement,
    2: document.getElementById("netpass-users-button") as HTMLButtonElement,
    3: document.getElementById("reports-button") as HTMLButtonElement,
    4: document.getElementById("activity-log-button") as HTMLButtonElement
}

for (let i = 1; i <= 4; i++) {
    showSectionButtons[i].addEventListener("click", () => showSection(i - 1))
}

const viewMoreButton = document.getElementById("audits-view-more") as HTMLButtonElement
viewMoreButton.addEventListener("click", () => showSection(3))

function showSection(sectionID: number) {
    for (let i = 0; i < sections.length; i++) {
        var section = document.getElementById(sections[i]) as HTMLDivElement;
        var navButton = document.getElementById(sections[i] + "-button") as HTMLButtonElement;
        if (i == sectionID && !section.classList.contains("hidden")) return;
        section.style.opacity = "0";
        section.style.transform = "translateY(50px)";
        if (i == sectionID) {
            section.classList.remove("hidden");
            navButton.classList.add("selected");
            var sectionToFade = section;
            setTimeout(() => {
                sectionToFade.style.opacity = "1";
                sectionToFade.style.transform = "translateY(0px)";
            }, 100);
        } else {
            section.classList.add("hidden");
            navButton.classList.remove("selected");
        }
    }
}

const collapseButton = document.getElementById("collapse-nav-button") as HTMLButtonElement;
collapseButton.addEventListener("click", toggleCollapseNav);

function toggleCollapseNav() {
    var navBar = document.getElementById("nav") as HTMLDivElement;
    navBar.classList.toggle("collapsed");
    var navLogo = document.getElementById("nav-logo") as HTMLImageElement;
    navLogo.src = navBar.classList.contains("collapsed")
        ? NetPassLogoNoText
        : NetPassLogo;
}

document.addEventListener("DOMContentLoaded", function () {
    fetch("/me")
        .then((response) => response.json())
        .then((data) => {
            let usernameText = document.getElementById("discord-username") as HTMLSpanElement;
            usernameText.innerText = `@${data.user.username}`;
            username = data.user.username;
        });
});
