export interface ClientReport {
    id: string;
    from_mac: string;
    reported_mac: string;
    report_message: string;
    messages: (WebReportPayload | null)[]
    closed: boolean;
    time: string;
}

// identical to WebReportPayload in src/moderation/cec/handlers/abstractCecHandler.ts
export interface WebReportPayload {
    titleId: number;
    titleName?: string;
    images?: string[];
    message?: string;
    replies?: {
        message: string;
        replyTo: string;
    }[]
    creatorName?: string;
    mii?: string;
}

